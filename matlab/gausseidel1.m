function [res] = gausseidel1(A, b, iter, init)
    n = length(A);
    x = init;
    res = zeros(n,iter);
    
    D = A.*eye(n);
    R = A-D;
    Dinv = inv(D);
    
    for I=1:iter
        for J=1:n
            x(J) = Dinv(J,J)*(b(J)-R(J,:)*x);
        end
        res(:,I) = x;
    end
end

