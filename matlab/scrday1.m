iter = 100;
n = 3;
A = [4 -1 -1; -2 6 1; -1 1 7];
b = [3;9;-6];
x = A\b;
init = [0;0;0];

res1 = jacobi1(A,b,iter, init);
res2 = gausseidel1(A,b,iter, init);

e1 = zeros(n,1);
e2 = zeros(n,1);

for I=1:iter
    e1(I) = norm(x-res1(:,I));
    e2(I) = norm(x-res2(:,I));
end

semilogy(1:iter,e1,'r')
hold on;
semilogy(1:iter,e2,'b')
