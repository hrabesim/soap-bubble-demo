function [res] = jacobi1(A, b, iter, init)
    n = length(A);
    x = init;
    res = zeros(n,iter);
    
    D = A.*eye(n);
    R = A-D;
    Dinv = inv(D);
    
    for I=1:iter
        x = Dinv*(b-R*x);
        res(:,I) = x;
    end
end

